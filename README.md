# CSC7342 Project

Project for the CSC7342 - Virtualization: Concept and Implementation course for the academic year 2021-2022.  
The purpose of this project is to deploy a MongoDB database instance using Kubernetes, such that it can be accessed from outside the cluster through a Mongo-Express interface that connects to this instance.

## Project Description

The project consists of six YAML files:  
- ***mongodb.yaml***: This file contains the specification for deploying the MongoDB application using Kubernetes. Only 1 replica is requested for the sake of simplicity, and the containers are set to use the latest image of the MongoDB application.  Default MongoDB port 27017 is used for internally exposing the MongoDB application through the corresponding Service. Specific environment variables are used for specifying the username and password of the database admin through the use of a Secret.
- ***mongo-express.yaml***: This file contains the specification for deploying the Mongo-Express interface application through the use of Kubernetes. Only 1 replica is requested for the sake of simplicity, and the containers are set to use the latest image of the Mongo-Express application. Port 8081 is used for externally exposing this web-based application through the corresponding Service. Specific environment variables are used for specifying the internal Service through which the MongoDB database application can be reached, as well as the username and the password of the database admin through the use of a Secret.
- ***mongodb-service.yaml***: This file is used for defining the Service that will be used to expose the MongoDB application. It is set to be of type ClusterIP, since it only needs to be exposed internally within the cluster (and more specifically, only to the Mongo-Express instance in our case). It exposes default MongoDB port 27017, and it links to Pods running MongoDB using the selector field.
- ***mongo-express-external-service.yaml***: This file describes the Service necessary for exposing the Mongo-Express interface to users outside the cluster. As such, it is set to be of type NodePort, and it exposes port 8081 of the Mongo-Express application chosen using the selector.
- ***mongo-secret.yaml***: This file defines the Secret containing the values for the database administrator username and password, used for authenticating this user upon connecting to our MongoDB application through Mongo-Express.
- ***mongo-config.yaml***: This file contains a definition for the ConfigMap required for storing necessary information for the Mongo-Express to connect to the internal Service linking it to the MongoDB.

***References for information regarding Docker images and applications' environment variables:***  
_https://hub.docker.com/_/mongo_  
_https://hub.docker.com/_/mongo-express_  
***References for information regarding default port numbers:***  
_https://docs.mongodb.com/manual/reference/default-mongodb-port/_  
_https://github.com/mongo-express/mongo-express_

## Project Deployment

In order to deploy this project, the following steps must be followed:  
First, we need to make sure that minikube and kubectl are installed (since these are the Kubernetes tools that we will be using in this particular example). Once this is done, we can then execute the following:  
We need to make sure that the Secret is created before the Pods are deployed. As such, we use the following command:  
`kubectl apply -f mongo-secret.yaml`  
Afterwards, create the ConfigMap:  
`kubectl apply -f mongo-config.yaml`  
Then, create the Services that will be used for linking the applications internally between them as well as to the outside of the cluster:  
`kubectl apply -f mongodb-service.yaml`  
`kubectl apply -f mongo-express-external-service.yaml`  
Finally, create the Pods corresponding to both our applications, that said MongoDB and Mongo-Express:  
`kubectl apply -f mongodb.yaml`  
`kubectl apply -f mongo-express.yaml`

In order to connect to the Mongo-Express interface from a web browser, first run the following command:  
`kubectl get services`  
From the output of this command, retrieve the port number allocated to this NodePort Service by the system. Then retrieve the IP Address of your minikube instance using the following command:  
`minikube ip`  
Finally, connect to the Mongo-Express instance from a web browser using the following command:  
`https://<retrieved_ip_of_the_minikube_instance>:<retrieved_NodePort_Service_port_value>`
